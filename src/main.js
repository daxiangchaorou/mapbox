/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2023-08-08 08:53:36
 * @LastEditTime: 2024-10-14 10:32:31
 * @LastEditors: Dxcr
 */
import "./assets/style/global.scss";

import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import ElementPlus from "element-plus";
import "element-plus/dist/index.css";

import {
  renderWithQiankun,
  qiankunWindow,
  QiankunProps,
} from "vite-plugin-qiankun/dist/helper";

const render = async (props = {}) => {
  const { container } = props;
  const app = createApp(App);

  app.use(createPinia());
  app.use(router);
  app.use(ElementPlus);
  app.mount(container?.querySelector("#app") || "#app");
};

const initQianKun = () => {
  renderWithQiankun({
    bootstrap() {
      console.log("微应用：bootstrap");
    },
    mount(props) {
      // 获取主应用传入数据
      console.log("微应用：mount", props);
      render(props);
    },
    unmount(props) {
      console.log("微应用：unmount", props);
    },
    update(props) {
      console.log("微应用：update", props);
    },
  });
};

qiankunWindow.__POWERED_BY_QIANKUN__ ? initQianKun() : render(); // 判断是否使用 qiankun ，保证项目可以独立运行
