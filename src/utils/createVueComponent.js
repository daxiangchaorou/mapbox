/*
 * @Description: 
 * @Author: Dxcr
 * @Date: 2024-09-05 09:12:37
 * @LastEditTime: 2024-09-05 09:12:42
 * @LastEditors: Dxcr
 */
/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2024-06-04 11:41:08
 * @LastEditTime: 2024-07-03 17:29:14
 * @LastEditors: Dxcr
 */
import { createApp } from "vue";

class CreateVueComponent {
  static createDom(VueComponent, options) {
    const props = options.props || {};
    const style = options.style || {};
    const dom = document.createElement("div");
    for (let styleName in style) {
      dom.style[styleName] = style[styleName];
    }
    const component = createApp(VueComponent, props);
    component.mount(dom);
    return { component, dom };
  }
}

export default CreateVueComponent;
