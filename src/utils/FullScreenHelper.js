/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2024-09-11 15:59:32
 * @LastEditTime: 2024-09-11 16:06:48
 * @LastEditors: Dxcr
 */
class FullScreenHelper {
  /**
   * 切换全屏模式：如果当前不是全屏则进入全屏，如果是全屏则退出全屏
   * @param {HTMLElement} element - 需要全屏的 HTML 元素，默认为 document.documentElement (整个页面)
   */
  static toggleFullScreen(element = document.documentElement) {
    if (this.isFullScreen()) {
      this.exitFullScreen();
    } else {
      this.enterFullScreen(element);
    }
  }

  /**
   * 进入全屏模式
   * @param {HTMLElement} element - 需要全屏的 HTML 元素，默认为 document.documentElement (整个页面)
   */
  static enterFullScreen(element = document.documentElement) {
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen(); // Firefox
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen(); // Chrome, Safari 和 Opera
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen(); // IE/Edge
    }
  }

  /**
   * 退出全屏模式
   */
  static exitFullScreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen(); // Firefox
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen(); // Chrome, Safari 和 Opera
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen(); // IE/Edge
    }
  }

  /**
   * 检测当前是否处于全屏模式
   * @returns {boolean} 返回是否处于全屏状态
   */
  static isFullScreen() {
    return !!(
      document.fullscreenElement ||
      document.mozFullScreenElement ||
      document.webkitFullscreenElement ||
      document.msFullscreenElement
    );
  }

  /**
   * 监听全屏状态变化
   * @param {Function} callback - 当全屏状态改变时触发的回调函数
   */
  static onFullScreenChange(callback) {
    document.addEventListener("fullscreenchange", callback);
    document.addEventListener("mozfullscreenchange", callback);
    document.addEventListener("webkitfullscreenchange", callback);
    document.addEventListener("msfullscreenchange", callback);
  }

  /**
   * 取消全屏状态变化的监听
   * @param {Function} callback - 移除的回调函数
   */
  static removeFullScreenChangeListener(callback) {
    document.removeEventListener("fullscreenchange", callback);
    document.removeEventListener("mozfullscreenchange", callback);
    document.removeEventListener("webkitfullscreenchange", callback);
    document.removeEventListener("msfullscreenchange", callback);
  }
}
export default FullScreenHelper;