/*
 * @Description: 
 * @Author: Dxcr
 * @Date: 2024-09-05 09:12:21
 * @LastEditTime: 2024-09-05 09:12:26
 * @LastEditors: Dxcr
 */
import mitt from 'mitt';
export default mitt();