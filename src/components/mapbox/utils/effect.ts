/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2022-12-07 16:25:05
 * @LastEditTime: 2024-09-09 09:16:10
 * @LastEditors: Dxcr
 */

/* turf 地理空间分析库，处理各种地图算法 */
import * as turf from "@turf/turf";
// 动效js
import TWEEN from "@tweenjs/tween.js";

// Setup the animation loop.
function animate(time) {
  requestAnimationFrame(animate);
  TWEEN.update(time);
}

let isAnimate = false;
function startRequestAnimationFrame() {
  if (isAnimate) {
    return;
  }
  requestAnimationFrame(animate);
  isAnimate = true;
}

class mapboxEffect {
  private map;
  constructor(map?) {
    this.map = map;
  }

  /**
   * @description:
   * @param {*} lineString gson lineString 数据
   * @param {*} source 对应地图资源
   * @param {*} option
   * @return {*}
   */
  waterFlowOnLineString(lineString, source, option) {
    let nc = { distance: 0 };
    //折线坐标数组
    let gsonCoords = turf.getCoords(lineString);

    //折线总计长度
    var length = turf.length(lineString, { units: "kilometers" });
    //总耗时
    let time = 5 * 1000;

    const tween = new TWEEN.Tween({ ...nc })
      .repeat(Infinity)
      .repeatDelay(100)
      .to({ distance: length }, time)
      .start()
      .onStop((object) => {
        console.log(nc);
        frame(nc);
      })
      .onUpdate((object) => {
        frame(object);
      });

    function frame(object) {
      //运动距离
      let distance = object.distance;
      //运动距离对应折线坐标点
      var sliced: any = turf.lineSliceAlong(lineString, 0, distance, {
        units: "kilometers",
      });

      // const chunkList = turf.lineChunk(sliced, 2, {
      //   units: "kilometers",
      // }).features;
      // let ChunksCollection = turf.featureCollection(chunkList);

      //找到对应的折线
      let data = source._data || turf.featureCollection([]);
      let features = data.features;
      let i = -1;
      features.find((object, index) => {
        if (object.properties.id == lineString.properties.id) {
          i = index;
          return true;
        }
      });
      if (i >= 0) {
        features[i] = sliced;
      } else {
        features.push(sliced);
      }
      source.setData(data);
    }

    //每帧加载
    startRequestAnimationFrame();

    return tween;
  }

  /**
   * @description:
   * @param {*} lineString gson lineString 数据
   * @param {*} source 对应地图资源
   * @param {*} time 时间
   * @return {*}
   */
  waterFlowOnLineString2(lineString, source, time) {
    console.log(lineString)
    let nc = { distance: 0 };
    //折线坐标数组
    let gsonCoords = turf.getCoords(lineString);

    //折线总计长度
    var length = turf.length(lineString, { units: "kilometers" });
    const tween = new TWEEN.Tween({ ...nc })
      // .repeat(0)
      // .repeatDelay(100)
      .to({ distance: length }, time)
      .start()
      .onStop((object) => {
        onStop(object)
      })
      .onUpdate((object) => {
        frame(object);
      });

    function frame(object) {
      if(!tween.isPlaying()){
        return
      }
      //运动距离
      let distance = object.distance;

      //以运动距离分割线段
      var sliced: any = turf.lineSliceAlong(lineString, 0, distance, {
        units: "kilometers",
      });

      sliced.properties = lineString.properties

      // const chunkList = turf.lineChunk(sliced, 2, {
      //   units: "kilometers",
      // }).features;
      // let ChunksCollection = turf.featureCollection(chunkList);

      //找到对应的折线
      let data = source._data || turf.featureCollection([]) || {};
      let features = data.features || [];
      let i = -1;
      features.find((object, index) => {
        if (object.properties.id == lineString.properties.id) {
          i = index;
          return true;
        }
      });
      if (i >= 0) {
        features[i] = sliced;
      } else {
        features.push(sliced);
      }
      console.log(data)
      source.setData(data);
    }

    let onStop = (object)=>{
      console.log('stop')
      // let data = source._data || turf.featureCollection([]) || {};
      // let features = data.features || [];
      // let i = -1;
      // features.filter((object, index) => {
      //   if (object.properties.id == lineString.properties.id) {
      //     i = index;
      //     return true;
      //   }
      // });
      // source.setData( turf.featureCollection([]));
    }

    //每帧加载
    startRequestAnimationFrame();

    return tween;
  }
}

function useMapboxEffect(map?) {
  return new mapboxEffect(map);
}

export default useMapboxEffect;
