/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2024-09-12 17:24:21
 * @LastEditTime: 2024-09-13 10:50:44
 * @LastEditors: Dxcr
 */
class MapStyle {
  constructor(mapStyle) {
    this.init();
    if(mapStyle.tianditu){
        this.tianditu()
    }
    if(mapStyle.offline){
        this.offline()
    }
  }
  init() {
    this.mapStyle = {
      version: 8,
      glyphs: "/glyphs/mapbox/{fontstack}/{range}.pbf",
      sources: {},
      layers: [],
    };
  }
  /**
   * @description: 天地图
   * @return {*}
   */
  tianditu() {
    let cvaUrl =
      "https://t0.tianditu.gov.cn/cia_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=cia&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca";
    let vecUrl =
      "https://t0.tianditu.gov.cn/img_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=img&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca";
    let cvaUrl1 =
      "https://t0.tianditu.gov.cn/cva_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=cva&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca";
    let vecUrl1 =
      "https://t0.tianditu.gov.cn/vec_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=vec&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca";

    let sources = {
      //天地图-卫星图
      td_satellite_CVA: {
        type: "raster",
        tiles: [`${vecUrl}`],
        tileSize: 256,
      },
      //天地图-卫星图标注
      td_satellite_Vec: {
        type: "raster",
        tiles: [`${cvaUrl}`],
        tileSize: 256,
      },
      //天地图-矢量图
      td_vetor_CVA: {
        type: "raster",
        tiles: [`${vecUrl1}`],
        tileSize: 256,
      },
      //天地图-矢量图标注
      td_vetor_Vec: {
        type: "raster",
        tiles: [`${cvaUrl1}`],
        tileSize: 256,
      },
    };

    let layers = [
      {
        id: "td_satellite_CVA",
        type: "raster",
        source: "td_satellite_CVA",
      },
      {
        id: "td_satellite_Vec",
        type: "raster",
        source: "td_satellite_Vec",
      },
      {
        id: "td_vetor_CVA",
        type: "raster",
        source: "td_vetor_CVA",
        layout: {
          visibility: "none",
        },
      },
      {
        id: "td_vetor_Vec",
        type: "raster",
        source: "td_vetor_Vec",
        layout: {
          visibility: "none",
        },
      },
    ];
    this.mergeStyle(sources,layers);
  }

  /**
   * @description: 离线地图
   * @return {*}
   */
  offline(
    url = "http://192.168.238.152:8701/maps/shandong/{z}/{x}/{y}.png"
  ) {
    let source = {
      offline: {
        type: "raster",
        tiles: [url],
        tileSize: 256,
      },
    };
    let layers = [
      {
        id: "offline",
        type: "raster",
        source: "offline",
      },
    ];
    this.mergeStyle(source, layers);
  }
  mergeStyle(sources, layers) {
    // 合并 sources 对象给 this.mapStyle.sources
    this.mapStyle.sources = Object.assign({}, this.mapStyle.sources, sources);
    // 合并 layers 对象给 this.mapStyle.layers
    this.mapStyle.layers = (this.mapStyle.layers || []).concat(layers);
  }

  getStyle(){
    return this.mapStyle;
  }
}
export default MapStyle