/*
 * @Description: 
 * @Author: Dxcr
 * @Date: 2024-09-11 16:31:48
 * @LastEditTime: 2024-09-12 15:37:54
 * @LastEditors: Dxcr
 */
import QWebChannel from './qwebchannel.js'
class QtHandler {
    constructor() {
      this.init();
    }
  
    init() {
      try {
        console.log("QT加载中...");
        if (window.qt && window.qt.webChannelTransport) {
          new QWebChannel(qt.webChannelTransport, (channel) => {
            window.bridge_js = channel.objects.bridge_name; // bridge_name 与 QT 中一致
            console.log("QT加载成功");
          });
        } else {
          throw new Error("QT WebChannelTransport 不存在");
        }
      } catch (error) {
        console.error("无 QT 或 QT 加载失败:", error);
      }
    }
  
    /**
     * 注册回调函数
     * @param {string} funcName - 函数名
     * @param {Function} func - 回调函数
     */
    on(funcName, func) {
      if (typeof funcName === 'string' && typeof func === 'function') {
        window[funcName] = func; // 注册全局函数
      } else {
        console.error("无效的参数");
      }
    }
  
    /**
     * 触发 QT 端函数
     * @param {string} funcName - 函数名
     * @param {...any} args - 传递给 QT 函数的参数
     */
    invoke(funcName, ...args) {
      if (window.bridge_js && typeof window.bridge_js[funcName] === 'function') {
        try {
          window.bridge_js[funcName](...args);
        } catch (error) {
          console.error(`调用 ${funcName} 时出错:`, error);
        }
      } else {
        console.error(`桥接函数 ${funcName} 不存在或未定义`);
      }
    }
  }

  export default QtHandler