/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2024-09-11 14:39:57
 * @LastEditTime: 2024-09-11 14:47:36
 * @LastEditors: Dxcr
 */
export interface Marker {
  position: [number, number];
  name: string;
  type: string;
  id: string;
}

export interface Pipeline {
  info: string;
  name: string;
  paths: [number, number][];
  id: string;
}

export interface Alarm {
  position: [number, number];
  type: string;
  name: string;
  id: string;
}

export interface DataStructure {
  pipelines: Pipeline[];
  markers: Marker[];
  stations: any[];
  alarms: Alarm[];
  planes: any[];
  camera: any[];
  areas: any[];
}
