/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2023-08-08 08:53:36
 * @LastEditTime: 2024-10-14 10:11:30
 * @LastEditors: Dxcr
 */
import {
  createRouter,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";

import { qiankunWindow } from "vite-plugin-qiankun/dist/helper";
const base = qiankunWindow.__POWERED_BY_QIANKUN__ ? "/mapbox" : "/"; //主应用的项目路径

const router = createRouter({
  history: createWebHistory(base),
  routes: [
    {
      path: "/",
      name: "/",
      // redirect:'/mapbox'
      component: () => import("../views/mapbox/index.vue"),
    },
    {
      path: "/index",
      name: "index",
      component: () => import("../views/index.vue"),
      // children: [
      //   {
      //     path: "",
      //     name: "index/index",
      //     component: () => import("../views/index/index.vue"),
      //   },
      // ],
    },
    {
      path: "/mapbox",
      name: "mapbox",
      component: () => import("../views/mapbox/index.vue"),
    },
  ],
});

export default router;
