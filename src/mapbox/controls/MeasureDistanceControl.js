/*
 * @Description: 
 * @Author: Dxcr
 * @Date: 2023-08-02 10:06:38
 * @LastEditTime: 2024-01-22 08:41:49
 * @LastEditors: Dxcr
 */
import { measureArea,closeMeasureArea } from '../utils/measureArea.js'
export default class MeasureDistanceControl {
    constructor(func){
        this._func = func
    }
  onAdd(map) {
    this._map = map;
    let dom = document.createElement("div");
    this._container = dom

    dom.className = "mapboxgl-ctrl mapboxgl-ctrl-group";
    dom.innerHTML = `<button class="mapboxgl-ctrl-measure" style="display: flex;justify-content: center;align-items: center;" type="button" title="">
    <svg t="1690943869199" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="8777" width="22" height="22"><path d="M880 112H144c-17.7 0-32 14.3-32 32v736c0 17.7 14.3 32 32 32h736c17.7 0 32-14.3 32-32V144c0-17.7-14.3-32-32-32z m-40 728H184V184h656v656z" p-id="8778" fill="#2c2c2c"></path><path d="M484 366h56c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8zM302 548h56c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8zM666 548h56c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8zM484 548h56c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8zM484 730h56c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8z" p-id="8779" fill="#2c2c2c"></path></svg>
    </button>`
    let f = false
    dom.addEventListener('click',()=>{
		
      if(f){
        closeMeasureArea(map)
      }else{
        measureArea(map)
      }
      f=!f
    })
    
    return dom;
  }

  onRemove() {
    this._container.parentNode.removeChild(this._container);
    this._map = undefined;
  }
}