export function formatDate(date, fmt) {
    //获取年份
    if (/(y+)/.test(fmt)) {
      	// 把数字变成字符串
      	let dateY = date.getFullYear() + "";
      	//RegExp.$1 在判断中出现过，且是括号括起来的，所以 RegExp.$1 就是 "yyyy"
      	fmt = fmt.replace(RegExp.$1, dateY.substr(4 - RegExp.$1.length));
    }
  
    //获取其他
    let o = {
		"M+": date.getMonth() + 1,
		"d+": date.getDate(),
		"h+": date.getHours(),
		"m+": date.getMinutes(),
		"s+": date.getSeconds(),
    }
	for(var k in o) {

        if(new RegExp("("+ k +")").test(fmt)){

             fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));

         }

    }
    return fmt;
}