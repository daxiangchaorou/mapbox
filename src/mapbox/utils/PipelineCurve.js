import {Curve, Vector3, SplineCurve} from 'three/build/three.module';

export default class PipelineCurve extends Curve {

    constructor(points) {

        super();
        this.pipeline = new SplineCurve(points);
    }

    getPoint(t, optionalTarget = new Vector3()) {

        let point = optionalTarget;
        let pos = this.pipeline.getPoint(t);

        point.set(pos.x, 0, pos.y);

        return point;
    }
}