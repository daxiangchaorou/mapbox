/*
 * @Description:
 * @Author: Dxcr
 * @Date: 2023-08-08 08:53:36
 * @LastEditTime: 2024-10-14 09:04:56
 * @LastEditors: Dxcr
 */
import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import qiankun from 'vite-plugin-qiankun'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    qiankun('mapbox', {
      useDevMode: true,
    }),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  build: {
    target:'es2015',
    outDir:'mapbox',
    brotliSize: false,
    chunkSizeWarningLimit: 1024,
  },
  css: {
    postcss: {
      plugins: [],
    },
  },
  server: {
    host: "0.0.0.0",
    hmr: {
      overlay: false,
    },
    port: 8001,
    // 是否自动在浏览器打开
    open: false,
    // 是否开启 https
    https: false,
    // proxy: {
    //   "/dev-api": {
    //     target: "http://192.168.16.34:8098",
    //     changeOrigin: true,
    //     rewrite: (path) => path.replace(/^\/dev-api/, ""),
    //   },
    // },
  },
});
